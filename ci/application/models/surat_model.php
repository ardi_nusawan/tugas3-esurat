<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_model extends CI_Model {
	function history(){
		$hasil = $this->db->get('history');
		return $hasil->result();
	}

	function edit($nomor){
		$this->db->where('id_history', $nomor);
		$hasil = $this->db->get('history');
		return $hasil->row();
	}

	function edithistory(){
		$config = array(
			'allowed_types' => 'jpg|jpeg|png|pdf|doc|docx',
			'upload_path' => 'images/',
			'max_size' => '5000'
		);
		$file_name = NULL;
		$this->load->library('upload');
		$this->upload->initialize($config);
		if($this->upload->do_upload('path')){
			$data2 = $this->upload->data();
			$file_name = $config['upload_path'].$data2['file_name'];
		}
		$data = $this->input->post();
		$data['path'] = $file_name;
		$this->db->insert('history', $data);
	}

	public function insertsurat(){
		$config = array(
			'allowed_types' => 'jpg|jpeg|png|pdf|doc|docx',
			'upload_path' => 'images/',
			'max_size' => '5000'
		);
		$file_name = NULL;
		$this->load->library('upload');
		$this->upload->initialize($config);
		if($this->upload->do_upload('path')){
			$data2 = $this->upload->data();
			$file_name = $config['upload_path'].$data2['file_name'];
		}

		$this->db->insert('buat_surat', $this->input->post());
		$nomor = $this->db->insert_id();
		$data = $this->input->post();
		$data['nomor_surat'] = $nomor;
		$data['path'] = $file_name;
		$this->db->insert('history', $data);
	}

	function gambar($id_surat){
		$query = $this->db->query('select path from buat_surat where nomor_surat = '.$id_surat);
		return $query->row();
	}

	function search($keyword)
	{
		$this->db->like('nomor_surat', $keyword);
		$this->db->or_like('lampiran', $keyword);
		$this->db->or_like('perihal', $keyword);
		$this->db->or_like('tipe', $keyword);
		$this->db->or_like('alamat_dibuat', $keyword);
		$this->db->or_like('tgl_dibuat', $keyword);
		$this->db->or_like('pengirim', $keyword);
		$this->db->or_like('penerima', $keyword);
		$this->db->or_like('jenis', $keyword);
		$this->db->or_like('instansi', $keyword);
		$this->db->or_like('isi', $keyword);
		$this->db->or_like('alamat_dituju', $keyword);

		$result = $this->db->get('history');

		return $result->result();
	}
}