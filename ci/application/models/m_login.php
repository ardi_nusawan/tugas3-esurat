<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_login extends CI_Model {
	function __construct(){
		$this->load->database();
	}

	function m_validasi($data){
		$hasil = $this->db->get_where('login', $data);
		return $hasil->num_rows();
	}
}