<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit</title>

	<!---------- CSS ------------>
    <base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/style4.css">

</head>

<body>

    <!--BEGIN #signup-form -->
    <div id="signup-form">
        
        <!--BEGIN #subscribe-inner -->
        <div id="signup-inner">
        
        	<div class="clearfix" id="header">
        
                <b><center style="font-size:38; margin-bottom:20px;">Pengarsipan</center></b>

            
            </div>
            <form id="send" method="post" action="home/inserthistory" enctype="multipart/form-data">
            	<div style="font-family: Arial; color: black;">
                  <span class="radio" style="margin-right: 60px;">
            <label style="display:inline;">
              <?php
              if($hasil->tipe=="Surat Masuk")
                echo '
              <input type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk" checked="checked" style="border:none; box-shadow:none; display:inline; width:auto;">';
              else
                echo '
                <input type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk" style="border:none; box-shadow:none; display:inline; width:auto;">';?>
                Surat Masuk
            </label>
          </span>
          <span class="radio">
            <label style="display:inline;">
              <?php
              if($hasil->tipe=="Surat Keluar")
                echo '
              <input type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar" checked="checked" style="border:none; box-shadow:none; display:inline; width:auto;">';
              else
                echo '
              <input type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar" style="border:none; box-shadow:none; display:inline; width:auto;">';?>
                Surat Keluar
            </label>
          </span>

                <p>
                <input id="company" type="hidden" name="nomor_surat" value="<?php echo $hasil->nomor_surat ?>"/>
                </p>

                <p>

                <label for="name">Lampiran</label>
                <input id="name" type="text" name="lampiran" value="<?php echo $hasil->lampiran ?>"/>
                </p>
                
                <p>
                <label for="company">Perihal</label>
                <input id="company" type="text" name="perihal" value="<?php echo $hasil->perihal ?>"/>
                </p>
                
                <p>

                <label for="name">Alamat Surat dibuat</label>
                <input id="name" type="text" name="alamat_dibuat" value="<?php echo $hasil->alamat_dibuat ?>"/>
                </p>
                
                <p>
                <label for="name">Tanggal Surat dibuat</label>
                <input id="name" type="date" name="tgl_dibuat" value="<?php echo $hasil->tgl_dibuat ?>"/>
                </p>
                
                <p>

                <label for="name">Nama Pengirim Surat</label>
                <input id="name" type="text" name="pengirim" value="<?php echo $hasil->pengirim ?>"/>
                </p>
                
                <p>
                <label for="name">Nama Penerima Surat</label>
                <input id="name" type="text" name="penerima" value="<?php echo $hasil->penerima ?>"/>
                </p>
                
                
                <p>
                <label for="name">Jenis Surat</label>
                <input id="name" type="text" name="jenis" value="<?php echo $hasil->jenis ?>"/>

                </p>

                <p>
                <label for="name">Nama Instansi</label>
                <input id="name" type="text" name="instansi" value="<?php echo $hasil->instansi ?>"/>
                </p>

                <p>
                <label for="profile">Isi Surat</label>
                <textarea name="isi" id="profile" cols="30" rows="10"/><?php echo $hasil->isi ?></textarea>
                </p>

                <p>
                <label for="name">Alamat Tujuan Surat</label>
                <input id="name" type="text" name="alamat_dituju" value="<?php echo $hasil->alamat_dituju ?>"/>
                </p>

                <br>
                <input id="name" type="file" name="path" value="<?php echo $hasil->path ?>"/>
              
                
                <p>

                <button id="submit" type="submit">Submit</button>
                </p>
                
            </form>
            </div>
        
        <!--END #signup-inner -->
        </div>
        
    <!--END #signup-form -->   
    </div>

</body>
</html>
