<html>
  <head>
  	<base href="<?php echo base_url(); ?>"></base>
    <link href='bootstrap/css/style2.css' rel='stylesheet'>
    <link href='bootstrap/css/js.js' rel='stylesheet'>
    <title>Pengarsipan</title>
  </head>
  <body>
    <div class="form-style-8">
      <h2 align="center">Input Data Surat</h2>
      <form method="post" action="home/insertsurat" enctype="multipart/form-data">
        <div style="font-family: Arial; color: white; margin-left:24%">
          <span class="radio" style="margin-right: 60px;">
            <label>
              <input type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk" checked>
                Surat Masuk
            </label>
          </span>
          <span class="radio">
            <label>
              <input type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar">
                Surat Keluar
            </label>
          </span>
        </div>
        </br>
        <input type="text" name="lampiran" placeholder="Lampiran" />
        <input type="text" name="perihal" placeholder="Perihal" />
        <input type="text" name="alamat_dibuat" placeholder="Alamat Surat dibuat" />
        <input type="date" name="tgl_dibuat" placeholder="Tanggal Surat dibuat" />
        <input type="text" name="pengirim" placeholder="Nama Pengirim Surat" />
        <input type="text" name="penerima" placeholder="Nama Penerima Surat" />
        <input type="text" name="jenis" placeholder="Jenis Surat" />
        <input type="text" name="instansi" placeholder="Nama Instansi" />
        <input type="text" name="isi" placeholder="Isi surat" />
        <input type="text" name="alamat_dituju" placeholder="Alamat Tujuan Surat" />
        <label>
        <input type="file" name="path" style="margin-left:24%;"/>
        <button type="submit" style="margin-left:24%;">Submit</button>
      </form>
    </div>
    
  </body>
</html>