<html>
  <head>
  	<base href="<?php echo base_url()?>"></base>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400;300' rel='stylesheet' type='text/css'>
    <link href='bootstrap/css/style.css' rel='stylesheet'>
    <title>History</title>
  </head>
  <body style="background-color:#091D1E;">

    <div class="menu">
      
      <!-- Menu icon -->
      <div class="icon-close">
        <img src="http://s3.amazonaws.com/codecademy-content/courses/ltp2/img/uber/close.png">
      </div>

      <!-- Menu -->
      <ul>
        <li><a href="home">Home</a></li>
        <li><a href="home/buatsurat">Pengarsipan</a></li>
        <li><a href="home/history">History</a></li>
        <li><a href="home/keluar">Log Out</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="jumbotron3" style="min-height:100%;">

      <div class="icon-menu" style="color: white;">    
        <i class="fa fa-bars"></i>
          Menu
      </div>
      <form class="form-inline" action="home/search" method="post" style="float:right; margin-right:5%; margin-bottom:1%;">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search" name="search">
          <button class="btn btn-default" type="submit">Search</button>
        </div>
      </form>
   

    
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/app.js"></script>
  </body>
</html>