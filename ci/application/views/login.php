<head>
	<title>Esurat</title>
	<base href="<?php echo base_url() ?>"></base>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>	

<body img src="asset/home.jpg" style="height: 100%; width: 100%; overflow:hidden;" />
	<form method="post" action="login/cek">
		<div class="container-fluid one-page-full">
			<?php
				if(isset($salah)){
					echo '
					<div class="col-xs-12 alert alert-warning" role="alert" style="position:fixed; left:-1px; text-align:center">
						Username atau password salah 
					</div>';
				}
				else if(isset($keluar))
					echo '
					<div class="col-xs-12 alert alert-success" role="alert" style="position:fixed; left:-1px; text-align:center">
						Log out berhasil 
					</div>';
			?>

			<div class="row row-centered">
				<div class="col-xs-9 col-md-4 col-centered foo">
						<h1 class="text-center">
							Log In
						</h1><br/>
						<div class="form-group">
							<input type="text" class="form-control input-lg" id="InputUsername" placeholder="NIP" name="nip">
						</div>
						<div class="form-group">
							<input type="password" class="form-control input-lg" id="InputPassword" placeholder="Password" name="password">
						</div>
						<button type="submit" class="btn btn-default btn-lg btn-block" style="float:right;">Log In</button>
				</div>
			</div>
		</div>
	</form>
</body>