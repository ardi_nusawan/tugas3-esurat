<html>
  <head>
  	<base href="<?php echo base_url()?>"></base>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400;300' rel='stylesheet' type='text/css'>
    <link href='bootstrap/css/style.css' rel='stylesheet'>
    <title>Home</title>
  </head>
  <body>

    <div class="menu">
      
      <!-- Menu icon -->
      <div class="icon-close">
        <img src="http://s3.amazonaws.com/codecademy-content/courses/ltp2/img/uber/close.png">
      </div>

      <!-- Menu -->
      <ul>
        <li><a href="home">Home</a></li>
        <li><a href="home/buatsurat">Pengarsipan</a></li>
        <li><a href="home/history">History</a></li>
        <li><a href="home/keluar">Log Out</a></li>
      </ul>
    </div>

    <!-- Main body -->
    <div class="jumbotron">

      <div class="icon-menu" style="color: white;">
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Menu
      </div>
    </div>
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bootstrap/js/app.js"></script>
  </body>
</html>