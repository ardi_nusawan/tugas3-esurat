<!DOCTYPE html>
<html>
	<head>
  		<base href="<?php echo base_url()?>"></base>
    	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
    	<style>
    		table { 
			   width: auto; 
			   word-wrap:break-word;
			  
			   
			}
    	</style>
	</head>

	<body>
		<div id="table-anto" style="margin-top:5%; overflow:auto">
			<table>
				<thead >
					<tr>
						<th style="width:4.3%;">Nomor Surat</th>
						<th style="width:6%;">Lampiran</th>
						<th style="width:5%;">Perihal</th>
						<th style="width:3.9%;">Tipe</th>
						<th style="width:5.3%;">Alamat dibuatnya Surat</th>
						<th style="width:8%;">Tanggal dibuatnya Surat</th>
						<th style="width:5%;">Pengirim Surat</th>
						<th style="width:5.3%;">Penerima Surat</th>
						<th style="width:6.5%;">Jenis Surat</th>
						
						<th style="width:5%;">Instansi</th>
						<th style="width:7%;">Alamat Tujuan Surat</th>
						<th style="width:5%;">Surat</th>
						<th style="width:1%;">Edit/Lihat isi</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($hasil as $jumlah): ?>
						<tr>
							<td><?php echo $jumlah->nomor_surat ?></td>
							<td><?php echo $jumlah->lampiran ?></td>
							<td><?php echo $jumlah->perihal ?></td>
							<td><?php echo $jumlah->tipe ?></td>
							<td><?php echo $jumlah->alamat_dibuat ?></td>
							<td><?php echo $jumlah->tgl_dibuat ?></td>
							<td><?php echo $jumlah->pengirim ?></td>
							<td><?php echo $jumlah->penerima ?></td>
							<td><?php echo $jumlah->jenis ?></td>
							
							<td><?php echo $jumlah->instansi ?></td>
							<td><?php echo $jumlah->alamat_dituju ?></td>
							<td><a href="<?php echo $jumlah->path ?>"><?php echo $jumlah->path ?></a></td>
							<td><a href="home/editsurat/<?php echo $jumlah->id_history ?>" class="label label-success"><span class="glyphicon glyphicon-file">Edit</span></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</body>
</html>