<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pengarsipan</title>

	<!---------- CSS ------------>
    <base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/style4.css">

</head>

<body>

    <!--BEGIN #signup-form -->
    <div id="signup-form">
        
        <!--BEGIN #subscribe-inner -->
        <div id="signup-inner">
        
        	<div class="clearfix" id="header">
        
                <b><center style="font-size:38; margin-bottom:20px;">Pengarsipan</center></b>

            
            </div>
            <form id="send" method="post" action="home/insertsurat" enctype="multipart/form-data">
            	<div style="font-family: Arial; color: black;">
                  <span style="display:inline;">
                    <label style="display:inline;">
                      <input style="border:none; box-shadow:none; display:inline; width:auto;" type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk" checked>
                        Surat Masuk
                    </label>
                  </span>
                  <span style="display:inline;">
                    <label style="display:inline;">
                      <input style="border:none; box-shadow:none; display:inline; width:auto;" type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar">
                        Surat Keluar
                    </label>
                  </span>
                </div>

                <p>

                <label for="name">Lampiran</label>
                <input id="name" type="text" name="lampiran" />
                </p>
                
                <p>
                <label for="company">Perihal</label>
                <input id="company" type="text" name="perihal" />
                </p>
                
                <p>

                <label for="name">Alamat Surat dibuat</label>
                <input id="name" type="text" name="alamat_dibuat" />
                </p>
                
                <p>
                <label for="name">Tanggal Surat dibuat</label>
                <input id="name" type="date" name="tgl_dibuat" />
                </p>
                
                <p>

                <label for="name">Nama Pengirim Surat</label>
                <input id="name" type="text" name="pengirim" />
                </p>
                
                <p>
                <label for="name">Nama Penerima Surat</label>
                <input id="name" type="text" name="penerima" />
                </p>
                
                
                <p>
                <label for="name">Jenis Surat</label>
                <input id="name" type="text" name="jenis" />

                </p>

                <p>
                <label for="name">Nama Instansi</label>
                <input id="name" type="text" name="instansi" />
                </p>

                <p>
                <label for="profile">Isi Surat *</label>
                <textarea name="isi" id="profile" cols="30" rows="10"></textarea>
                </p>

                <p>
                <label for="name">Alamat Tujuan Surat</label>
                <input id="name" type="text" name="alamat_dituju" />
                </p>

                <br><input id="name" type="file" name="path" />
              
                
                <p>

                <button id="submit" type="submit">Submit</button>
                </p>

                <br>Note * : Maksimum 20 karakter, jika melebihi data diatas ketentuan tidak akan diperlihatkan.
                
            </form>
            </div>
        
        <!--END #signup-inner -->
        </div>
        
    <!--END #signup-form -->   
    </div>

</body>
</html>
