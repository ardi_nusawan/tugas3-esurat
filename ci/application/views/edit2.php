<html>
  <head>
  	<base href="<?php echo base_url()?>"></base>
    <link href='bootstrap/css/style2.css' rel='stylesheet'>
    <link href='bootstrap/css/js.js' rel='stylesheet'>
    
  </head>
  <body>
    <div class="form-style-8">
      <h2 align="center">Input Data Surat</h2>
      <form method="post" action="home/inserthistory">
        <div style="font-family: Arial; color: white; margin-left:24%">
          <span class="radio" style="margin-right: 60px;">
            <label>
              <?php
              if($hasil->tipe=="Surat Masuk")
                echo '
              <input type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk" checked="checked">';
              else
                echo '
                <input type="radio" name="tipe" id="optionsRadios1" value="Surat Masuk">';?>
                Surat Masuk
            </label>
          </span>
          <span class="radio">
            <label>
              <?php
              if($hasil->tipe=="Surat Keluar")
                echo '
              <input type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar" checked="checked">';
              else
                echo '
              <input type="radio" name="tipe" id="optionsRadios2" value="Surat Keluar">';?>
                Surat Keluar
            </label>
          </span>
        </div>
        </br>
        <input type="hidden" name="nomor_surat" value="<?php echo $hasil->nomor_surat ?>" />
        <input type="text" name="lampiran" placeholder="Lampiran" value="<?php echo $hasil->lampiran ?>" />
        <input type="text" name="perihal" placeholder="Perihal" value="<?php echo $hasil->perihal ?>" />
        <input type="text" name="alamat_dibuat" placeholder="Alamat Surat dibuat" value="<?php echo $hasil->alamat_dibuat ?>" />
        <input type="date" name="tgl_dibuat" placeholder="Tanggal Surat dibuat" value="<?php echo $hasil->tgl_dibuat ?>" />
        <input type="text" name="pengirim" placeholder="Nama Pengirim Surat" value="<?php echo $hasil->pengirim ?>" />
        <input type="text" name="penerima" placeholder="Nama Penerima Surat" value="<?php echo $hasil->penerima ?>" />
        <input type="text" name="jenis" placeholder="Jenis Surat" value="<?php echo $hasil->jenis ?>" />
        <input type="text" name="instansi" placeholder="Nama Instansi" value="<?php echo $hasil->instansi ?>" />
        <input type="text" name="isi" placeholder="Isi surat" value="<?php echo $hasil->isi ?>" />
        <input type="text" name="alamat_dituju" placeholder="Alamat Tujuan Surat" value="<?php echo $hasil->alamat_dituju ?>" />
        <input type="text" name="path" value="<?php echo $hasil->path ?>" />
        <button type="submit">Submit</button>
      </form>
    </div>
    
  </body>
</html>