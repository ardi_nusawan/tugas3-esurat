<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$sid=$this->session->userdata('status');
		$admin = $this->session->userdata('username');
		if(!$sid){
			if($admin=="admin")
				redirect('home/admin');
			else
				redirect('login');

		}
	}

	public function admin(){
		$this->load->view('home_admin');
	}

	public function anggota(){
		$this->load->view('home_anggota');
		$this->load->view('anggota');
	}

	public function salahnip(){
		$this->load->view('salahnip');
	}

	public function register(){
		$this->load->view('home_anggota');
		
		$this->load->model('login_model');

		$data=array(
			'username'=>$this->input->post('username'),
			'nip'=>$this->input->post('nip'),
			'status'=>TRUE
		);

		$hasil = $this->login_model->insertanggota();
		if($hasil == 0){
			$this->db->insert('login', $this->input->post());
			redirect('home/admin');
		}
		else{
			$data['salah']=TRUE;
			$this->load->view('anggota', $data);
		}
	}

	public function index(){
		$this->load->view('home');
	}

	public function buatsurat(){
		$this->load->view('homesurat');
		$this->load->view('buatsurat');
	}

	public function history(){
		$this->load->view('homehistory');
		$this->load->model('surat_model');
		$data['hasil'] = $this->surat_model->history();
		$this->load->view('history', $data);
	}

	public function editsurat($nomor){
		$this->load->view('homesurat');
		$this->load->model('surat_model');
		$data['hasil'] = $this->surat_model->edit($nomor);
		$this->load->view('edit', $data);
	}

	public function inserthistory(){
		$this->load->model('surat_model');
		$this->surat_model->edithistory();
		redirect('home/history');
	}

	public function keluar(){
		$this->session->sess_destroy();
		$data['keluar']=TRUE;
		$this->load->view('login', $data);
	}

	public function insertsurat(){
		$this->load->model('surat_model');
		$this->surat_model->insertsurat();
		redirect('home/history');
	}

	public function download($image){
		header("Content-type: image/");
		header("Content-Dispotition: attachment; filename={$image}");
		header("Content-length: " . filesize($image));
		header("pragma: no-cache");
		header("Expires: 0");
		readfile("{$image}");
	}

	public function search(){
		$this->load->view('homehistory');
		$this->load->model('surat_model');
		$key = $this->input->post('search');
		$result=$this->surat_model->search($key);
		$data['hasil'] = $result;

		$this->load->view('history', $data);
	}
}