<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$sid=$this->session->userdata('status');
		if($sid)
			redirect('home');
		else
			$this->load->view('login');
	}
	
	public function cek(){
		$this->load->model('login_model');
		$data=array(
			'nip'=>$this->input->post('nip'),
			'status'=>TRUE
		);

		$hasil = $this->login_model->cek();
		if($hasil == 1){
			$this->load->library('session');
			$hasil=$this->session->set_userdata($data);
			if($data['nip']=='00000')
				redirect('home/admin');
			else
				redirect('home');
		}
		else{
			$data['salah']=TRUE;
			$this->load->view('login', $data);
		}
	}
}