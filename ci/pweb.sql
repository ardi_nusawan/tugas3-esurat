-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2015 at 06:44 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `buat_surat`
--

CREATE TABLE IF NOT EXISTS `buat_surat` (
`nomor_surat` int(11) NOT NULL,
  `lampiran` varchar(50) DEFAULT NULL,
  `perihal` varchar(50) DEFAULT NULL,
  `tipe` varchar(15) DEFAULT NULL,
  `alamat_dibuat` varchar(50) DEFAULT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `pengirim` varchar(50) DEFAULT NULL,
  `penerima` varchar(50) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `instansi` varchar(50) DEFAULT NULL,
  `isi` text,
  `alamat_dituju` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `upload` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buat_surat`
--

INSERT INTO `buat_surat` (`nomor_surat`, `lampiran`, `perihal`, `tipe`, `alamat_dibuat`, `tgl_dibuat`, `pengirim`, `penerima`, `jenis`, `instansi`, `isi`, `alamat_dituju`, `path`, `upload`) VALUES
(1, 'test', 'test', 'Surat Masuk', 'Surabaya', '2015-05-07', 'coba1', 'coba2', 'resmi', 'ITS', 'percobaan membuat surat', 'Malang', NULL, NULL),
(2, 'test2', 'test2', 'Surat Masuk', 'Jakarta', '2015-05-14', 'percobaan1', 'percobaan2', 'undangan', 'ITB', 'percobaan kirim surat 2', 'Bali', NULL, NULL),
(3, '1 Berkas Lampiran', 'Penugasan', 'Surat Masuk', 'LP 1 Teknik Informatika ITS Surabaya', '2015-05-20', 'Ardi Nusawan', 'M. Razi', 'Surat Resmi', 'ITS Surabaya', 'Menugaskan peserta mengikuti lomba Gemnastik 7 di Universitas Gadjah Mada pada tanggal 22 Agustus - 27 Novermber 2014', 'ITS Surabaya', NULL, NULL),
(4, '1 Berkas Proposal', 'Permohonan', 'Surat Masuk', 'Teknik Informatika ITS Surabaya', '2014-01-09', 'Mukhlis Ndoyo Said', 'Sekolah Terkait IRITS 2014', 'Resmi', 'ITS Surabaya', 'Sehubungan dengan akan diselenggarakannya acara Integrated Roadshow ITS (IRITS) 2014 oleh Badan Eksekutif Mahasiswa (BEM) ITS pada:   hari / tanggal  : (Ditentukan panitia) waktu   : (Ditentukan panitia) tempat   : (Sekolah Terkait)    maka dengan ini kami selaku panitia Integrated Roadshow ITS (IRITS) 2014 bermaksud untuk memohon izin untuk melakukan roadshow di sekolah Bapak/Ibu untuk mendukung kelancaran acara tersebut. ', 'Sekolah Terkait IRITS 2014', NULL, NULL),
(5, '1 Berkas Proposal', 'Permohonan', 'Surat Masuk', 'Ruang Dosen', '2015-05-20', 'Ardi', 'Dwi', 'Rahasia', 'ITS', 'sdsaad', 'Mulyosari', NULL, NULL),
(6, '1 Berkas Proposal', 'Permohonan', 'Surat Keluar', 'LP 1 Teknik Informatika ITS Surabaya', '2015-05-21', 'dadas', 'sdasd', 'Pribadi', 'ITS', 'dasd', 'dad', NULL, NULL),
(7, '3 Berkas', 'Undangan', 'Surat Masuk', 'ITS Surabaya', '2015-05-26', 'Ardi Nusawan', 'Afif', 'Rahasia', 'ITS Surabaya', 'Mengundang bapak atau ibu untuk datang ke acara kami', 'Jalan Teknik Mesin Sukolilo Jawa Timur', NULL, NULL),
(8, '2', 'Info Lomba', 'Surat Keluar', 'Mulyorejo,Jawa Timur', '1996-03-01', 'Ardi Nusawan', 'Dwi Pratama', 'Biasa', 'ITS Surabaya', 'Kami ingin memberikan informasi bahwa akan diadakan lomba pada tanggal 10 nopember 45. Untuk selengkapnya, slahkan lihat pada file attached', 'Universitas Surabaya', NULL, NULL),
(9, '1', 'Undangan', 'Surat Masuk', 'Surabaya', '2015-05-28', 'wawan', 'dwi', 'Pribadi', 'ITS', 'daakfdfdfsdfsjdkfjksdnfjksnvjksdvnjksnjnsjdfjsdfsdf', 'Jakarta', NULL, NULL),
(10, '3 Berkas', 'surat lamaran', 'Surat Keluar', 'surabaya', '2015-05-01', 'Wawan', 'Wicak', 'Pribadi', 'TPKH ITS', 'Cak, kirimin aku surat lamaran KP untuk Telkom Indonesia, segera', 'AJK', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
`id_history` int(11) NOT NULL,
  `nomor_surat` int(11) DEFAULT NULL,
  `lampiran` varchar(50) DEFAULT NULL,
  `perihal` varchar(50) DEFAULT NULL,
  `tipe` varchar(15) DEFAULT NULL,
  `alamat_dibuat` varchar(50) DEFAULT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `pengirim` varchar(50) DEFAULT NULL,
  `penerima` varchar(50) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `instansi` varchar(50) DEFAULT NULL,
  `isi` text,
  `alamat_dituju` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `upload` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id_history`, `nomor_surat`, `lampiran`, `perihal`, `tipe`, `alamat_dibuat`, `tgl_dibuat`, `pengirim`, `penerima`, `jenis`, `instansi`, `isi`, `alamat_dituju`, `path`, `upload`) VALUES
(15, 9, '1', 'Undangan', 'Surat Masuk', 'Surabaya', '2015-05-28', 'wawan', 'dwi', 'Pribadi', 'ITS', 'daakfdfdfsdfsjdkfjksdnfjksnvjksdvnjksnjnsjdfjsdfsdf', 'Jakarta', 'images/Surat_Penugasan.jpg', NULL),
(17, 9, '1', 'Undangan', 'Surat Masuk', 'Surabaya', '2015-05-28', 'wawan', 'dwi', 'Pribadi', 'ITS', 'daakfdfdfsdfsjdkfjksdnfjksnvjksdvnjksnjnsjdfjsdfsdf', 'Jakarta', 'images/Surat-Permohonan-Roadshow.pdf', NULL),
(18, 10, '3 Berkas', 'surat lamaran', 'Surat Keluar', 'surabaya', '2015-05-01', 'Wawan', 'Wicak', 'Pribadi', 'TPKH ITS', 'Cak, kirimin aku surat lamaran KP untuk Telkom Indonesia, segera', 'AJK', 'images/Kuesioner_Dosen.jpg', NULL),
(19, 10, '3 Berkas', 'surat lamaran', 'Surat Keluar', 'surabaya', '2015-05-01', 'Wawan', 'Wicak', 'Pribadi', 'TPKH ITS', 'Cak, kirimin aku surat lamaran KP untuk Telkom Indonesia, segera', 'AJK', 'images/HMIF.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL,
  `NIP` decimal(18,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`USERNAME`, `PASSWORD`, `NIP`) VALUES
('admin', 'admin', '0'),
(NULL, 'wawan', '96'),
(NULL, 'zikrul', '97');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buat_surat`
--
ALTER TABLE `buat_surat`
 ADD PRIMARY KEY (`nomor_surat`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
 ADD PRIMARY KEY (`id_history`), ADD KEY `FK_history` (`nomor_surat`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`NIP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buat_surat`
--
ALTER TABLE `buat_surat`
MODIFY `nomor_surat` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
